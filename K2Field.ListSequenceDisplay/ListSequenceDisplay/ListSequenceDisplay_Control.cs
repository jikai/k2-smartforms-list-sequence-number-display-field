﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//SourceCode.Forms.Controls.Web.SDK.dll, located in the GAC of the smartforms server or in the bin folder of the K2 Designer web site
using SourceCode.Forms.Controls.Web.SDK;
using SourceCode.Forms.Controls.Web.SDK.Attributes;

//adds the client-side .js file as a web resource
[assembly: WebResource("K2Field.ListSequenceDisplay.ListSequenceDisplay.ListSequenceDisplay_Script.js", "text/javascript", PerformSubstitution = true)]
//adds the client-side style sheet as a web resource
[assembly: WebResource("K2Field.ListSequenceDisplay.ListSequenceDisplay.ListSequenceDisplay_Stylesheet.css", "text/css", PerformSubstitution = true)]
[assembly: WebResource("K2Field.ListSequenceDisplay.icon.png", "image/png")]
namespace K2Field.ListSequenceDisplay.ListSequenceDisplay
{
    //specifies the location of the embedded definition xml file for the control
    [ControlTypeDefinition("K2Field.ListSequenceDisplay.ListSequenceDisplay.ListSequenceDisplay_Definition.xml")]
    //specifies the location of the embedded client-side javascript file for the control
    [ClientScript("K2Field.ListSequenceDisplay.ListSequenceDisplay.ListSequenceDisplay_Script.js")]
    //specifies the location of the embedded client-side stylesheet for the control
    [ClientCss("K2Field.ListSequenceDisplay.ListSequenceDisplay.ListSequenceDisplay_Stylesheet.css")]
    //specifies the location of the client-side resource file for the control. You will need to add a resource file to the project properties
    //[ClientResources("K2Field.ListSequenceDisplay.Resources.[ResrouceFileName]")]
    public class Control : BaseControl
    {
        #region Control Properties

        #region IDs
        public string ControlID
        {
            get
            {
                return base.ID;
            }
            set
            {
                base.ID = value;
            }
        }

        public override string ClientID
        {
            get
            {
                return base.ID;
            }
        }

        public override string UniqueID
        {
            get
            {
                return base.ID;
            }
        }
        #endregion

        #endregion

        #region Contructor
        public Control()
            : base("div")  
        {
            ((SourceCode.Forms.Controls.Web.Shared.IControl)this).DesignFormattingPaths.Add("stlyecss", "K2Field.ListSequenceDisplay.ListSequenceDisplay.ListSequenceDisplay_Stylesheet.css");
        }
        #endregion

        #region Control Methods
        protected override void CreateChildControls()
        {
            base.EnsureChildControls();

            //Perform state-specific operations
            switch (base.State)
            {
                case SourceCode.Forms.Controls.Web.Shared.ControlState.Designtime:
                    //assign a temp unique Id for the control
                    this.ID = Guid.NewGuid().ToString();
                    Label lbl = new Label();
                    lbl.ID = this.ControlID + "_ListSeqDisplay";
                    lbl.Text = "List Sequence Display: Ensure 1st column is a Data Label field.";
                    this.Controls.Add(lbl);
                    break;
                case SourceCode.Forms.Controls.Web.Shared.ControlState.Preview:
                    //do any Preview-time manipulation here
                    break;
                case SourceCode.Forms.Controls.Web.Shared.ControlState.Runtime:
                    //do any runtime manipulation here
                    break;
            }

            // Call base implementation last
            base.CreateChildControls();
        }
        #endregion
    }
}
