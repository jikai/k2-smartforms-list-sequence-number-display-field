﻿//NOTE: alert() statements are available for debugging purposes. You can uncomment the statements to show dialogs when each method is hit.
(function ($) {
    //TODO: if necessary, add additional statements to initialize each part of the namespace before your ListSequenceDisplay is called. 
    if (typeof K2Field == "undefined" || K2Field == null) K2Field = {};
    if (typeof K2Field.ListSequenceDisplay === "undefined" || K2Field.ListSequenceDisplay == null) K2Field.ListSequenceDisplay = {};

    K2Field.ListSequenceDisplay.ListSequenceDisplay = {
        //internal method used to get a handle on the control instance
        _getInstance: function (id) {
            //alert("_getInstance(" + id + ")");
            var control = jQuery('#' + id);
            if (control.length == 0) {
                throw 'ListSequenceDisplay \'' + id + '\' not found';
            } else {
                return control[0];
            }
        },

        getValue: function (objInfo) {
            //alert("getValue() for control " + objInfo.CurrentControlId);
            var instance = K2Field.ListSequenceDisplay.ListSequenceDisplay._getInstance(objInfo.CurrentControlId);
            return instance.value;
        },

        getDefaultValue: function (objInfo) {
            //alert("getDefaultValue() for control " + objInfo.CurrentControlId);
            getValue(objInfo);
        },

        setValue: function (objInfo) {
            //alert("setValue() for control " + objInfo.CurrentControlId);
            var instance = K2Field.ListSequenceDisplay.ListSequenceDisplay._getInstance(objInfo.CurrentControlId);
            var oldValue = instance.value;
            //only change the value if it has actually changed, and then raise the OnChange event
            if (oldValue != objInfo.Value) {
                instance.value = objInfo.Value;
                raiseEvent(objInfo.CurrentControlId, 'Control', 'OnChange');
            }
        },

        //retrieve a property for the control
        getProperty: function (objInfo) {
            //alert("getProperty(" + objInfo.property + ") for control " + objInfo.CurrentControlId);
            if (objInfo.property.toLowerCase() == "value") {
                return K2Field.ListSequenceDisplay.ListSequenceDisplay.getValue(objInfo);
            }
            else {
                return $('#' + objInfo.CurrentControlId).data(objInfo.property);
            }
        },

        //set a property for the control. note case statement to call helper methods
        setProperty: function (objInfo) {
            switch (objInfo.property.toLowerCase()) {
                //case "isenabled":
                //    K2Field.ListSequenceDisplay.ListSequenceDisplay.setIsEnabled(objInfo);
                //    break;
                default:
                    $('#' + objInfo.CurrentControlId).data(objInfo.property).value = objInfo.Value;
            }
        },

        validate: function (objInfo) {
            //alert("validate for control " + objInfo.CurrentControlId);
        },

        refreshList: function (objInfo){
            if (!checkExists(objInfo)) return;

            var parentIDString = objInfo.CurrentControlID.split('_')[0];
            if (parentIDString.match(/^00000000-/)) { //this is a view
                viewHandle = $('div.grid');
            }
            else { //this is a form
                viewHandle = $('div.grid[id*="' + parentIDString + '"]');
            }
            this.printSequence(viewHandle[0].id, objInfo.CurrentControlID);
        },

        attachEvents: function (objInfo) {
            if (!checkExists(objInfo)) return;

            //var context = this._getInstance(objInfo.CurrentControlID);

            //get view handler
            var viewHandle = '';

            var parentIDString = objInfo.CurrentControlID.split('_')[0];
            if (parentIDString.match(/^00000000-/))
            { //this is a view
                viewHandle = $('div.grid');
            }
            else
            { //this is a form
                viewHandle = $('div.grid[id*="' + parentIDString + '"]');
            }

            //For IE, only works >= 11
            var Observer = window.MutationObserver || window.WebKitMutationObserver;
            var myObserver = new Observer(K2Field.ListSequenceDisplay.ListSequenceDisplay.watchAttributeChange);
            var obsConfig;

            //mutation observer
            // pagesize will refresh on column sort, 
            // listrefreshed when list refreshes
            // actiontype when there's editing
            obsConfig = { childList: false, characterData: false, attributes: true, attributeFilter: ['pagesize', 'listrefreshed', 'actiontype'], attributeOldValue: true, subtree: false };
            myObserver.observe($('#' + viewHandle[0].id)[0], obsConfig);

            var pageSize = viewHandle.attr('pagesize');
            if (pageSize === undefined)
            {
                //no paging, need to attach to the column header
                obsConfig = { childList: false, characterData: false, attributes: true, attributeFilter: ['class'], attributeOldValue: true, subtree: false };

                $('#' + viewHandle[0].id + ' .grid-column-header-table div.grid-column-header-cell').each(function () {
                    this.setAttribute('viewID', viewHandle[0].id); //insert for id later.
                    myObserver.observe(this, obsConfig);
                });
            }

            //run for the first time
            this.printSequence(viewHandle[0].id, objInfo.CurrentControlID);
        },

        watchAttributeChange: function(mutationRecords)
        {
            var found = 0;
            var viewID;
            var i = 0;
            while (i < mutationRecords.length && found == 0)
            {
                switch(mutationRecords[i].attributeName.toLowerCase())
                {
                    case 'pagesize':
                        found = 1;
                        viewID = mutationRecords[i].target.id;
                        break;

                    case 'listrefreshed':
                    case 'actiontype':
                        if (mutationRecords[i].oldValue != null)
                        { //prevent executing twice
                            found = 1;
                            viewID = mutationRecords[i].target.id;
                        }
                        break;

                    case 'class':
                        found = 1;
                        viewID = mutationRecords[i].target.getAttribute('viewID');
                        break;
                }
                i++;
            }

            if (found == 1)
            {
                var ctrlID = $('#' + viewID + ' .SFC.K2Field-ListSequenceDisplay-ListSequenceDisplay-Control')[0].id;
                K2Field.ListSequenceDisplay.ListSequenceDisplay.printSequence(viewID, ctrlID);
            }
        },

        printSequence: function (viewID, ctrlID) {
            if (viewID == "undefined" || viewID == null) return;

            var context = this._getInstance(ctrlID);

            //get view handler
            var viewHandle = $('div.grid[id*="' + viewID + '"]');

            var maxCounter = viewHandle.attr('maxcounter');
            var pageNumber = viewHandle.attr('pagenumber');
            var pageSize = viewHandle.attr('pagesize');
            
            if (pageNumber !== undefined)
            { //with paging
                var startNo = (parseInt(pageNumber) * parseInt(pageSize)) - parseInt(pageSize) + 1;

                for (i = 0; i < parseInt(pageSize) ; i++) {
                    var rowNo = parseInt(startNo) + i;
                    var rowHandle = viewHandle.find('.grid-content-table tr:nth-child(' + (i + 1) + ') td:nth-child(1) .grid-content-cell-wrapper');
                    if (rowHandle.length > 0)
                        rowHandle.text(rowNo);
                }
            }
            else
            {
                //when without paging and in editable mode, the maxcounter is not a reliable value
                //for (i = 1; i <= parseInt(maxCounter) ; i++) {
                pageSize = viewHandle.find('.grid-content-table tr[class!="action-row"]').length;

                for (i = 1; i <= parseInt(pageSize) ; i++) {
                    var rowHandle = viewHandle.find('.grid-content-table tbody > tr:not(:has(.action-row)):nth-child(' + i + ') td:nth-child(1) .grid-content-cell-wrapper');
                    if (rowHandle.length > 0)
                        rowHandle.text(i);
                }
            }
        },

        execute: function (objInfo) {
            if (!checkExists(objInfo)) return;

            var method = objInfo.methodName;
            switch (method) {
                case 'AttachEvent':
                    K2Field.ListSequenceDisplay.ListSequenceDisplay.attachEvents(objInfo);
                    break;

                case 'RefreshList':
                    K2Field.ListSequenceDisplay.ListSequenceDisplay.refreshList(objInfo);
                    break;
            }
        }
    };
})(jQuery);
