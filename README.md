# README #

A K2 smartforms control that helps populate the first column in a list view with running sequence number. The sequence will refresh when sorting is applied, page moved or jumped. See document for more detail.


## Tested Compatibility ##

Should work for all Chrome, Firefox and Safari, since they implements MutationObserver


### Tested K2 smartforms version ###

* 1.0.7 => OK

* 4.6.9 => TODO

* 4.6.10 => TODO

* 4.6.11 => OK


### Tested IE version ###

* IE 11 running in Edge document mode. (Meaning to say, only possible on K2 smartforms 4.6.9 and above based on compatibility matrix)